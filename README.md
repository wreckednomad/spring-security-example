****************************
**SPRING-SECURITY-EXAMPLE**
****************************

**Spring Security Features**
LDAP (Lightweight Directory Access Protocol)
Single sign-on
JAAS (Java Authentication and Authorization Service) LoginModule
Basic Access Authentication
Digest Access Authentication
Remember-me
Web Form Authentication
Authorization
Software Localization
HTTP Authorization
